/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.shapeproject;

/**
 *
 * @author Thanya
 */
public class Rectangle {
    private double w,l;
    public Rectangle(double w,double l){
        this.w =w;
        this.l = l;
    }
    public double calArea(){
        return w*l;
    }
    public double getW(){
        return w;   
    }
    public double getL(){
        return l;   
    }
    public void setWL(double w,double l){
       if(w <= 0 || l <= 0){
           System.out.println("Error: width and length must more than zero!!!!");
           return;
       }
       this.w = w;
       this.l = l;
    }
}
