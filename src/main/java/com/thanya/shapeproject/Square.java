/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.shapeproject;

/**
 *
 * @author Thanya
 */
public class Square {
    private double a;
    public Square(double a){
        this.a = a;
    }
    public double calArea(){
        return a*a;
    }
    public double getA(){
        return a;
    }
    public void setA(double a){
        if(a <= 0){
            System.out.println("Error: Width must more than zero!!!!");
            return;
        }
        this.a = a;
    }
        
}
