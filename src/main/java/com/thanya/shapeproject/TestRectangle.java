/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.shapeproject;

/**
 *
 * @author Thanya
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(2,5);
        System.out.println("Area of rectangle (w = "+rectangle1.getW()+" l = "+rectangle1.getL()+") is "+rectangle1.calArea());
        rectangle1.setWL(4.3,6);
        System.out.println("Area of rectangle (w = "+rectangle1.getW()+" l = "+rectangle1.getL()+") is "+rectangle1.calArea());
        rectangle1.setWL(0,0);
        System.out.println("Area of rectangle (w = "+rectangle1.getW()+" l = "+rectangle1.getL()+") is "+rectangle1.calArea());
        rectangle1.setWL(1,0);
        System.out.println("Area of rectangle (w = "+rectangle1.getW()+" l = "+rectangle1.getL()+") is "+rectangle1.calArea());
        rectangle1.setWL(0,1);
        System.out.println("Area of rectangle (w = "+rectangle1.getW()+" l = "+rectangle1.getL()+") is "+rectangle1.calArea());
    }
}
