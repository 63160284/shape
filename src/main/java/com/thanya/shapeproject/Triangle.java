/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.shapeproject;

/**
 *
 * @author Thanya
 */
public class Triangle {
    private double b,h;
    public static final double a = 1/2.0;
    public Triangle(double b,double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return a*b*h;
    }
    public double getB(){
        return b;
    }
    public double getH(){
        return h;
    }
    public void setBH(double b,double h){
        if(b <= 0 || h <= 0){
            System.out.println("Error: Base and Height must more than zero!!!!");
            return;
        }
        this.b = b;
        this.h = h;
    }
}
